import { KEY_AUTH } from '../config/Server';
import ErrorCodes from '../commons/ErrorCodes';
import UnauthorizedError from '../errors/unauthorized';
import ForbiddenError from '../errors/forbidden';
import { Role } from '../commons/Types';
import { verifyToken } from '../utils/jwt';

class RoleMiddleware {
  async roleAdmin(req, res, next) {
    const token = req.headers[KEY_AUTH] || null;
    try {
      if (!token) {
        throw new UnauthorizedError(ErrorCodes.REQUIRE_ACCESS_TOKEN);
      }
      const payload = verifyToken(token);
      if (payload.role !== Role.Admin) {
        throw new ForbiddenError(ErrorCodes.NOT_ALLOWED);
      }
      req.user = payload;
      next();
    } catch (error) {
      res.onError(error);
    }
  }

  async roleStaff(req, res, next) {
    const token = req.headers[KEY_AUTH] || null;
    try {
      if (!token) {
        throw new UnauthorizedError(ErrorCodes.REQUIRE_ACCESS_TOKEN);
      }
      const payload = verifyToken(token);
      if (payload.role === Role.User) {
        throw new ForbiddenError(ErrorCodes.NOT_ALLOWED);
      }
      req.user = payload;
      next();
    } catch (error) {
      res.onError(error);
    }
  }

  async roleUser(req, res, next) {
    const token = req.headers[KEY_AUTH] || null;
    try {
      if (!token) {
        throw new UnauthorizedError(ErrorCodes.REQUIRE_ACCESS_TOKEN);
      }
      const payload = verifyToken(token);
      if (!payload) {
        throw new ForbiddenError(ErrorCodes.NOT_ALLOWED);
      }
      req.user = payload;
      next();
    } catch (error) {
      res.onError(error);
    }
  }
}

export default new RoleMiddleware();
