export const PORT = 9000;

// use for bcrypt
export const SALT_WORK_FACTOR = 10;

// use for jwt
export const JWT_SECRET = 'xxxx_333_111_xxx33300';

export const JWT_OPTIONS = {
  expiresIn: '10 days',
  audience: 'fb.com/rum.nguyen',
  issuer: 'rum123ken@gmail.com',
};

export const KEY_AUTH = 'access-token';
