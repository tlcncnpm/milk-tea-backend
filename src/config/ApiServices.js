const apis = {
  Contact: process.env.URL_SERVER_CONTACT || 'http://localhost:3005/v1',
};

export default apis;
