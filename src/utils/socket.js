class SocketInstance {
  constructor(enforcer) {
    throw new Error('Cannot construct singleton');
  }
}

SocketInstance.io = 'SocketInstance';

export default SocketInstance;