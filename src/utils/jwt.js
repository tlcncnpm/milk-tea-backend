import jwt from 'jsonwebtoken';

import { JWT_SECRET, JWT_OPTIONS } from '../config/Server';

/**
 *
 * @param {Object} payload
 * @return {String} token
 */
export const generateToken = payload => jwt.sign(payload, JWT_SECRET, JWT_OPTIONS);

export const verifyToken = token => jwt.verify(token, JWT_SECRET);
