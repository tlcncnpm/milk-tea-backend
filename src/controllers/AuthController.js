// import libs

// import handlers
import AuthHandler from "../handlers/AuthHandler";
// import services

// import Error Codes
import ErrorCodes from "../commons/ErrorCodes";
// import utils
import { generateToken, verifyToken } from "../utils/jwt";
// import types
import {} from "../commons/Types";
// import another type
import { KEY_AUTH } from "../config/Server";
// import error types
import UnauthorizedError from "../errors/unauthorized";
import ValidateError from "../errors/validation";
import AlreadyError from "../errors/already-exists";
import NotFoundError from "../errors/not-found";
import { urlGoogle, getGoogleAccountFromCode } from "../utils/google";
import NotImplementedError from "../errors/not-implemented";
import AdminHandler from "../handlers/AdminHandler";

import fs from "fs";
import path from "path";

class AuthController {
    async login(req, res) {
        const { phone, password } = req.body;
        try {
            // validate input
            if (!phone) {
                throw new ValidateError(ErrorCodes.USERNAME_IS_EMPTY);
            }
            if (!password) {
                throw new ValidateError(ErrorCodes.PASSWORD_IS_EMPTY);
            }
            let user = null;
            user = await AuthHandler.getUserByPhone(phone);
            if (!user) {
                throw new NotFoundError(ErrorCodes.NOT_EXISTED_THIS_USERNAME);
            }
            // compare password
            const isMatchPassword = await user.comparePassword(password);
            if (!isMatchPassword) {
                throw new UnauthorizedError(ErrorCodes.INCORRECT_PASSWORD);
            }
            // create token
            const jwt = generateToken(AuthHandler.getPayloadJwtSchema(user));
            // return record user
            delete user.password;
            res.onSuccess({ jwt, user });
        } catch (error) {
            res.onError(error);
        }
    }

    async getGoogleLoginUrl(req, res) {
        res.redirect(urlGoogle());
    }

    async loginGoogle(req, res) {
        const { code } = req.query;
        try {
            const { email, avatar_url, name } = await getGoogleAccountFromCode(
                code
            );
            const user = await AuthHandler.getUserByEmail(email);
            if (user) {
                const jwt = generateToken(
                    AuthHandler.getPayloadJwtSchema(user)
                );
                return res.onSuccess({ jwt, is_new_account: false });
            }
            const newUser = await AuthHandler.createNewUser({
                email,
                role: "user",
                avatar_url,
                name,
            });
            const jwt = generateToken(AuthHandler.getPayloadJwtSchema(newUser));
            res.onSuccess({ jwt, is_new_account: true });
        } catch (error) {
            res.onError(error);
        }
    }

    async loginFacebook(req, res) {
        const { id, displayName } = req.user;
        try {
            const user = await AuthHandler.getUserByFacebookId(id);
            if (user) {
                const jwt = generateToken(
                    AuthHandler.getPayloadJwtSchema(user)
                );
                return res.onSuccess({ jwt, is_new_account: false });
            }
            const newUser = await AuthHandler.createNewUser({
                facebook_id: id,
                role: "user",
                name: displayName,
            });
            const jwt = generateToken(AuthHandler.getPayloadJwtSchema(newUser));
            res.onSuccess({ jwt, is_new_account: true });
        } catch (error) {
            res.onError(error);
        }
    }

    async register(req, res) {
        const { username, phone, password } = req.body;
        try {
            // Validator input
            if (!username) {
                throw new ValidateError(ErrorCodes.USERNAME_IS_EMPTY);
            }
            if (!phone) {
                throw new ValidateError(ErrorCodes.USERNAME_IS_EMPTY);
            }
            if (!password) {
                throw new ValidateError(ErrorCodes.PASSWORD_IS_EMPTY);
            } else if (password.length < 6) {
                throw new ValidateError(ErrorCodes.LENGTH_PASSWORD_LESS_THAN_6);
            }
            const user = await AuthHandler.getUserByPhone(phone);
            if (user) {
                throw new AlreadyError(ErrorCodes.ALREADY_HAVE_THIS_USER);
            }
            const newUser = await AuthHandler.createNewUser({
                username,
                phone,
                password,
                role: "user",
            });
            const jwt = generateToken(AuthHandler.getPayloadJwtSchema(newUser));
            res.onSuccess({ jwt });
        } catch (error) {
            res.onError(error);
        }
    }

    async updatePassword(req, res) {
        const { userId, password, newPassword } = req.body;
        try {
            if (!userId) {
                throw new ValidateError(ErrorCodes.USER_ID_IS_EMPTY);
            }
            let user = null;
            user = await AuthHandler.getUserById(userId);
            if (!user) {
                throw new NotFoundError(ErrorCodes.NOT_FOUND);
            }
            // compare password
            const isMatchPassword = await user.comparePassword(password);
            if (!isMatchPassword) {
                throw new UnauthorizedError(ErrorCodes.INCORRECT_PASSWORD);
            }
            const userUpdate = await AdminHandler.updateUser(userId, {
                password: newPassword,
            });
            if (userUpdate) {
                res.onSuccess("UPDATE_ACCOUNT_SUCCESS");
            } else {
                throw new NotImplementedError("UPDATE_ACCOUNT_ERROR");
            }
        } catch (error) {
            res.onError(error);
        }
    }

    async getUserInfo(req, res) {
        const token = req.headers[KEY_AUTH] || null;
        try {
            if (!token) {
                throw new UnauthorizedError(ErrorCodes.REQUIRE_ACCESS_TOKEN);
            }
            const payload = verifyToken(token);
            let user = null;
            user = await AuthHandler.getUserById(payload.userId);
            res.onSuccess({ username: user.username, role: user.role });
        } catch (error) {
            res.onError(error);
        }
    }

    getImage(req, res) {
        const imagePath = path.join(__dirname, "../../public", req.url);
        try {
            const fileStream = fs.createReadStream(imagePath);
            fileStream.on("error", function(err) {
                console.log(err);
            });
            res.writeHead(200, { "Content-type": "image/jpg" });
            fileStream.pipe(res);
        } catch (error) {
            res.onError(error);
        }
    }
}

export default new AuthController();
