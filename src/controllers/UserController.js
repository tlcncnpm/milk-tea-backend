// import libs

// import handlers
import UserHandler from "../handlers/UserHandler";
// import services

// import Error Codes
import ErrorCodes from "../commons/ErrorCodes";
// import utils
// import types
import {
    KeyConfig,
    DefaultValue,
    SocketMessage,
    SocketStatus,
} from "../commons/Types";
import { Pagination } from "../commons/Types";
import { ORDER_STATUS } from "../commons/Const";
// import another type
// import error types
import UnauthorizedError from "../errors/unauthorized";
import ValidateError from "../errors/validation";
import AlreadyError from "../errors/already-exists";
import NotFoundError from "../errors/not-found";
import NotImplementedError from "../errors/not-implemented";
import AdminHandler from "../handlers/AdminHandler";
import AuthHandler from "../handlers/AuthHandler";
import SocketInstance from "../utils/socket";

class UserController {
    async getProfile(req, res) {
        const { userId } = req.user;
        try {
            const user = await UserHandler.getAccount(userId);
            res.onSuccess({
                username: user.username,
                role: user.role,
                phone: user.phone,
                email: user.email,
                address: user.address,
                last_login: user.last_login,
                cart: user.cart,
                name: user.name,
                avatar_url: user.avatar_url,
            });
        } catch (error) {
            res.onError(error);
        }
    }

    async updateProfile(req, res) {
        const { userId } = req.user;
        const { password, phone, address, email } = req.body;
        try {
            const user = await AuthHandler.getUserByPhone(phone);
            if (user && user._id !== userId) {
                throw new NotImplementedError("UPDATE_PROFILE_ERROR");
            } else {
                const userUpdate = await UserHandler.updateProfile(userId, {
                    password,
                    phone,
                    address,
                    email,
                });
                if (userUpdate) {
                    res.onSuccess("UPDATE_PROFILE_SUCCESS");
                } else {
                    throw new NotImplementedError("UPDATE_PROFILE_ERROR");
                }
            }
        } catch (error) {
            res.onError(error);
        }
    }

    async getCategories(req, res) {
        const { page, pageSize } = req.query;
        try {
            if (pageSize > Pagination.perPageLimit) {
                throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
            }
            const categories = await UserHandler.getCategories(page, pageSize);
            res.onSuccess(categories);
        } catch (error) {
            res.onError(error);
        }
    }

    async getNotifications(req, res) {
        const { page, pageSize } = req.query;
        try {
            if (pageSize > Pagination.perPageLimit) {
                throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
            }
            const notifications = await UserHandler.getNotifications(
                page,
                pageSize
            );
            res.onSuccess(notifications);
        } catch (error) {
            res.onError(error);
        }
    }

    async createOrder(req, res) {
        const { userId } = req.user;
        const {
            address,
            lat_addr,
            long_addr,
            promotion_id,
            note,
            drinks,
        } = req.body;
        try {
            const feePrice = await UserHandler.caculatePrice(
                drinks,
                promotion_id,
                lat_addr,
                long_addr
            );
            const newOrder = await UserHandler.createOrder({
                user_id: userId,
                address,
                lat_addr,
                long_addr,
                promotion_id,
                note,
                drinks,
                fee_price: {
                    fee_ship_per_km: feePrice.feeShipPerKm,
                    distance: feePrice.totalDistance,
                    total_price: feePrice.totalPrice,
                    total_fee_ship: feePrice.totalFeeShip,
                    promotion_value: feePrice.promotionValue,
                    final_price: feePrice.finalPrice,
                },
            });
            SocketInstance.io.to("staff_room").emit("new_order", {
                ...newOrder,
                message: SocketMessage.NewOrder,
                status: SocketStatus.Info,
            });
            res.onSuccess("CREATE_ORDER_SUCCESS");
        } catch (error) {
            res.onError(error);
        }
    }

    async updateOrder(req, res) {
        const {
            orderId,
            address,
            lat_addr,
            long_addr,
            promotion_id,
            note,
            drinks,
            status,
        } = req.body;
        try {
            const orderUpdate = await AdminHandler.updateOrder(orderId, {
                address,
                lat_addr,
                long_addr,
                promotion_id,
                note,
                drinks,
                status,
            });
            if (orderUpdate) {
                res.onSuccess("UPDATE_ORDER_SUCCESS");
            } else {
                throw new NotImplementedError("UPDATE_ORDER_ERROR");
            }
        } catch (error) {
            res.onError(error);
        }
    }

    async getOrders(req, res) {
        const { userId } = req.user;
        const { page, pageSize } = req.query;
        try {
            if (pageSize > Pagination.perPageLimit) {
                throw new ValidateError(ErrorCodes.PAGE_SIZE_INVALID);
            }
            const orders = await AdminHandler.getOrdersOfUser(
                page,
                pageSize,
                userId
            );
            res.onSuccess(orders);
        } catch (error) {
            res.onError(error);
        }
    }

    async caculatePrice(req, res) {
        const { lat_addr, long_addr, promotion_id, drinks } = req.body;
        try {
            if (!lat_addr) {
                throw new NotFoundError(ErrorCodes.LATITUDE_IS_EMPTY);
            }
            if (!long_addr) {
                throw new NotFoundError(ErrorCodes.LONGITUDE_IS_EMPTY);
            }
            if (!drinks) {
                throw new NotFoundError(ErrorCodes.DRINKS_IS_EMPTY);
            }
            const calculatedPrice = await UserHandler.caculatePrice(
                drinks,
                promotion_id,
                lat_addr,
                long_addr
            );
            res.onSuccess({
                fee_ship_per_km: calculatedPrice.feeShipPerKm,
                distance: calculatedPrice.totalDistance,
                total_price: calculatedPrice.totalPrice,
                total_fee_ship: calculatedPrice.totalFeeShip,
                promotion_value: calculatedPrice.promotionValue,
                final_price: calculatedPrice.finalPrice,
            });
        } catch (error) {
            res.onError(error);
        }
    }
}

export default new UserController();
