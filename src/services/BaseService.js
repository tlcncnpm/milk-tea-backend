import Fetch from '../utils/fetch';

class BaseService {
  constructor(nameService = '') {
    this.name = nameService;
    this.fetch = Fetch(nameService);
  }
}

export default BaseService;
