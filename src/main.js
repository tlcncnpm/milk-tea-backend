import http from 'http';
import { userInfo } from 'os';
import BootMongo from './boots/BootMongo';
import BootExpress from './boots/BootExpress';
import { PORT } from './config/Server';
import SocketInstance from './utils/socket';
import { Role } from './commons/Types';
import { verifyToken } from './utils/jwt';

const runApp = async () => {
  try {
    await BootMongo();
    // Run services

    const app = BootExpress();

    const server = http.createServer(app);

    const io = require('socket.io')(server);

    io.on('connection', (socket) => {
      console.log('connected socket ', socket.id);

      socket.auth = false;
      socket.on('authenticate', (data) => {
        let payload;
        try {
          payload = verifyToken(data.token);
          console.log('Authenticated socket ', socket.id);
        } catch (error) {
          payload = null;
          console.log('Error authenticate socket ', socket.id);
        }
        if (payload) {
          socket.auth = true;
          if (payload.role === Role.User) {
            socket.join('user_room_' + payload.userId);
            console.log(
              'socket joined ',
              socket.id,
              'user_room_' + payload.userId,
            );
            const nspSockets = io.in('user_room_' + payload.userId)
              .sockets;
            console.log(
              'number socket in room',
              Object.keys(nspSockets).length,
            );
          } else if (payload.role === Role.Staff) {
            socket.join('staff_room');
            console.log('socket joined ', socket.id, 'staff_room');
            const nspSockets = io.in('staff_room').sockets;
            console.log(
              'number socket in room',
              Object.keys(nspSockets).length,
            );
          } else if (payload.role === Role.Admin) {
            socket.join('admin_room');
            console.log('socket joined ', socket.id, 'admin_room');
            const nspSockets = io.in('admin_room').sockets;
            console.log(
              'number socket in room',
              Object.keys(nspSockets).length,
            );
          }
          // ============ test socket ========================
          socket.emit('hello', {
            message: 'Welcome to crush milk tea! ',
            status: 'success',
          });
        }
        console.log('=====================');
      });

      // setTimeout(function () {
      //   if (!socket.auth) {
      //     console.log("disconnecting socket ", socket.id);
      //     socket.disconnect('unauthorized');
      //     console.log('=====================');
      //   }
      // }, 5000);

      socket.on('disconnect', (data) => {
        console.log('disconnected socket ' + data);
      });
    });

    SocketInstance.io = io;

    server.listen(PORT, () => console.log(`Listening on port ${PORT}`));
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

runApp();
