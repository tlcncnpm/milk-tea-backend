import AuthController from '../../controllers/AuthController';

const router = require('express').Router();

router.get('/images/:name', AuthController.getImage);

export default router;
