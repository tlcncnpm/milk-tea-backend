import AdminController from "../../controllers/AdminController";
import RoleMiddleware from "../../middlewares/RoleMiddleware";

const router = require("express").Router();

// router.use('/*', RoleMiddleware.roleStaff);
router.use("/*", RoleMiddleware.roleAdmin);

// account
router.post("/account", AdminController.createAccount);
router.put("/account", AdminController.updateAccount);
router.delete("/account", AdminController.deleteAccount);
router.get("/account", AdminController.getAccounts);
router.get("/account/:userId", AdminController.getAccount);
router.get("/profile", AdminController.getProfile);

// cateogry

router.post("/category", AdminController.createCategory);
router.put("/category", AdminController.updateCategory);
router.delete("/category", AdminController.deleteCategory);
router.get("/category", AdminController.getCategories);
router.get("/category/:categoryId", AdminController.getCategory);

// main drinks

router.post("/main_drink", AdminController.createMainDrink);
router.put("/main_drink", AdminController.updateMainDrinks);
router.delete("/main_drink", AdminController.deleteMainDrink);
router.get("/main_drink/:categoryId", AdminController.getMainDrinks);
router.get(
    "/main_drink/:categoryId/:mainDrinkId",
    AdminController.getMainDrinkById
);

// topping

router.post("/topping", AdminController.createTopping);
router.put("/topping", AdminController.updateTopping);
router.delete("/topping", AdminController.deleteTopping);
router.get("/topping", AdminController.getToppings);
router.get("/topping/:toppingId", AdminController.getToppingById);

// notification

router.post("/notification", AdminController.createNotification);
router.put("/notification", AdminController.updateNotification);
router.delete("/notification", AdminController.deleteNotification);
router.get("/notification", AdminController.getNotifications);

// promotion
router.post("/promotion", AdminController.createPromotion);
router.put("/promotion", AdminController.updatePromotion);
router.delete("/promotion", AdminController.deletePromotion);
router.get("/promotion", AdminController.getPromotions);

// config
router.post("/config", AdminController.createConfig);
router.put("/config", AdminController.updateConfig);
router.delete("/config", AdminController.deleteConfig);
router.get("/config", AdminController.getConfigs);

// order
router.put("/order", AdminController.updateOrder);
router.delete("/order", AdminController.deleteOrder);
router.get("/order", AdminController.getOrders);
router.get("/order/:userId", AdminController.getOrdersOfUser);
router.get("/order/detail/:orderId", AdminController.getOrderDetail);

// upload_image
router.post("/upload", AdminController.uploadImage);

export default router;
