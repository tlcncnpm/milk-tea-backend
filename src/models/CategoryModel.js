import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const CategorySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  code: {
    type: String,
    required: true,
  },
  image_url: {
    type: String,
  },
  main_drinks: [{
    name: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    rating: {
      type: Number,
      default: 0
    },
    image_url: {
      type: String,
    },
    is_active: {
      type: Boolean,
      default: true,
    },
  }],
}, { versionKey: false });

CategorySchema.plugin(mongoosePaginate);

export default mongoose.model('Category', CategorySchema);
