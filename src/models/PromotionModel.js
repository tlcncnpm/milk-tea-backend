import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const PromotionSchema = new Schema({
  code: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
  },
  image_url: {
    type: String,
  },
  value: {
    type: Number,
    required: true,
  },
  time_start: {
    type: Date,
    default: Date.now(),
  },
  time_end: {
    type: Date,
    required: true,
  },
}, { versionKey: false });
PromotionSchema.plugin(mongoosePaginate);
export default mongoose.model('Promotion', PromotionSchema);
