import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import { OrderState } from '../commons/Types'

const OrderSchema = new Schema({
  user_id: {
    type: String,
    required: true,
  },
  shipper_id: {
    type: String
  },
  status: {
    type: String,
    default: OrderState.Ordered,
  },
  address: {
    type: String,
    required: true,
  },
  lat_addr: {
    type: Number,
    required: true
  },
  long_addr: {
    type: Number,
    required: true
  },
  promotion_id: {
    type: String,
  },
  ordered_time: {
    type: Date,
    default: Date.now()
  },
  confirmed_time: {
    type: Date,
  },
  to_delivery_time: {
    type: Date,
  },
  completed_time: {
    type: Date,
  },
  canceled_time: {
    type: Date,
  },
  note: {
    type: String,
  },
  fee_price: {
    type: Object,
  },
  drinks: [{
    category_id: {
      type: String,
      required: true,
    },
    main_drink_id: {
      type: String,
      required: true,
    },
    toppings: [String],
    quantity: {
      type: Number,
      default: 1,
    },
  }],
}, { versionKey: false });

OrderSchema.plugin(mongoosePaginate);

export default mongoose.model('Order', OrderSchema);
