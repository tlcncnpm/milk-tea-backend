import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const NotificationSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    content: {
      type: String,
    },
    image_url: {
      type: String,
    },
    time_start: {
      type: Date,
      default: Date.now(),
    },
    time_end: {
      type: Date,
    },
  },
  { versionKey: false },
);

NotificationSchema.plugin(mongoosePaginate);
export default mongoose.model('Notification', NotificationSchema);
