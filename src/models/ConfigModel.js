import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const ConfigSchema = new Schema({
  key: {
    type: String,
    required: true,
  },
  value: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  }
}, { versionKey: false });
ConfigSchema.plugin(mongoosePaginate);
export default mongoose.model('Config', ConfigSchema);
