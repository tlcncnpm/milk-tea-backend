import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const ToppingSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  code: {
    type: String,
    required: true,
  },
  image_url: {
    type: String,
  },
  price: {
    type: Number,
    required: true,
  },
  is_active: {
    type: Boolean,
    default: true,
  },
});

ToppingSchema.plugin(mongoosePaginate);

export default mongoose.model('Topping', ToppingSchema);
