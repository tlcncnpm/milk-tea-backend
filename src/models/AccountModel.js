import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import { hashText, compareTextAndHash } from '../utils/bcrypt';


const AccountSchema = new Schema({
  username: {
    type: String,
  },
  password: {
    type: String,
  },
  role: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
  },
  facebook_id: {
    type: String,
  },
  email: {
    type: String,
  },
  address: {
    type: String,
  },
  avatar_url: {
    type: String,
  },
  name: {
    type: String,
  },
  last_login: {
    type: Date,
  },
  cart: [{
    main_drink_id: {
      type: String,
      required: true,
    },
    toppings: [{
      topping_id: {
        type: String,
        required: true,
      },
    }],
    quantity: {
      type: Number,
      default: 1,
    },
  }],
}, { versionKey: false });

async function preSaveAccountModel(cb) {
  const user = this;
  if (!user.isModified('password')) return cb();
  try {
    const hash = await hashText(user.password);
    user.password = hash;
    return cb();
  } catch (error) {
    return cb(error);
  }
}

async function comparePasswordMethod(candidatePassword) {
  const isMatch = await compareTextAndHash(candidatePassword, this.password);
  return isMatch;
}

AccountSchema.pre('save', preSaveAccountModel);
AccountSchema.methods.comparePassword = comparePasswordMethod;

AccountSchema.plugin(mongoosePaginate);

export default mongoose.model('Account', AccountSchema);
