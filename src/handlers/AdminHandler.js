import AccountModel from "../models/AccountModel";
import {
    Pagination,
    KeyConfig,
    DefaultValue,
    OrderState,
} from "../commons/Types";
import CategoryModel from "../models/CategoryModel";
import ToppingModel from "../models/ToppingModel";
import NotificationModel from "../models/NotificationModel";
import PromotionModel from "../models/PromotionModel";
import ConfigModel from "../models/ConfigModel";
import OrderModel from "../models/OrderModel";
import UserHandler from "./UserHandler";

class AdminHandler {
    async getUserByUsername(username) {
        const user = await AccountModel.findOne({ username });
        return user;
    }

    createNewUser(data) {
        return AccountModel.create(data);
    }

    async updateUser(userId, data) {
        const {
            password,
            role,
            phone,
            address,
            email,
            last_login,
            username,
        } = data;
        const user = await AccountModel.findById(userId);
        if (!user) return null;
        if (password) {
            user.set({ password });
        }
        if (role) {
            user.set({ role });
        }
        if (phone) {
            user.set({ phone });
        }
        if (address) {
            user.set({ address });
        }
        if (email) {
            user.set({ email });
        }
        if (last_login) {
            user.set({ last_login });
        }
        if (username) {
            user.set({ username });
        }
        const userUpdate = await user.save();
        return userUpdate;
    }

    async deleteAccount(userId) {
        const deleted = await AccountModel.deleteOne({
            _id: userId,
        });
        if (deleted.deletedCount === 1 || deleted.ok === 1) {
            return true;
        }
        return false;
    }

    async getAccount(userId) {
        const user = await AccountModel.findOne({ _id: userId });
        return user;
    }

    async getAccounts(page, pageSize) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit,
            select: "username role phone address email last_login",
        };
        const accounts = await AccountModel.paginate({}, options);
        return accounts;
    }

    async getCategory(code) {
        const category = await CategoryModel.findOne({ code });
        return category;
    }

    async getCategoryById(categoryId) {
        const category = await CategoryModel.findById(categoryId);
        return category;
    }

    createNewCategory(data) {
        return CategoryModel.create(data);
    }

    async updateCategory(categoryId, data) {
        const { name, image_url } = data;
        const category = await CategoryModel.findById(categoryId);
        if (!category) return null;
        if (name) {
            category.set({ name });
        }
        if (image_url) {
            category.set({ image_url });
        }
        const categoryUpdate = await category.save();
        return categoryUpdate;
    }

    async deleteCategory(categoryId) {
        const deleted = await CategoryModel.deleteOne({
            _id: categoryId,
        });
        if (deleted.deletedCount === 1 || deleted.ok === 1) {
            return true;
        }
        return false;
    }

    async getCategories(page, pageSize) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit,
            select: "code image_url name ",
        };
        const categories = await CategoryModel.paginate({}, options);
        return categories;
    }

    async createMainDrink(categoryId, data) {
        const updated = await CategoryModel.updateOne(
            { _id: categoryId },
            { $push: { main_drinks: data } }
        );
        if (updated.nModified === 1) {
            return true;
        }
        return false;
    }

    async updateMainDrink(categoryId, mainDrinkId, data) {
        const { name, price, rating, is_active, image_url } = data;
        const category = await CategoryModel.findOne({
            _id: categoryId,
        });
        if (!category) {
            return false;
        }
        let position = -1;
        for (let i = 0; i < category.main_drinks.length; i++) {
            if (mainDrinkId == category.main_drinks[i]._id) {
                position = i;
                break;
            }
        }
        if (position == -1) {
            return false;
        }
        if (name) {
            category.main_drinks[position].set({ name });
        }
        if (price) {
            category.main_drinks[position].set({ price });
        }
        if (rating) {
            category.main_drinks[position].set({ rating });
        }
        if (is_active || is_active === false) {
            category.main_drinks[position].set({ is_active });
        }
        if (image_url) {
            category.main_drinks[position].set({ image_url });
        }
        const isUpdated = await category.save();
        return isUpdated;
    }

    async deleteMainDrink(categoryId, mainDrinkId) {
        const updated = await CategoryModel.updateOne(
            { _id: categoryId },
            { $pull: { main_drinks: { _id: mainDrinkId } } }
        );
        if (updated.nModified === 1 || deleted.ok === 1) {
            return true;
        }
        return false;
    }

    async getMainDrink(categoryId, mainDrinkId) {
        const category = await this.getCategoryById(categoryId);
        if (!category) {
            return null;
        }
        for (let i = 0; i < category.main_drinks.length; i++) {
            if (category.main_drinks[i]._id == mainDrinkId) {
                return {
                    _id: category.main_drinks[i]._id,
                    name: category.main_drinks[i].name,
                    price: category.main_drinks[i].price,
                    image_url: category.main_drinks[i].image_url,
                };
            }
        }
        return null;
    }

    createNewTopping(data) {
        return ToppingModel.create(data);
    }

    async updateTopping(toppingId, data) {
        const { name, image_url, price, is_active } = data;
        const topping = await ToppingModel.findById(toppingId);
        if (!topping) return null;
        if (name) {
            topping.set({ name });
        }
        if (image_url) {
            topping.set({ image_url });
        }
        if (price) {
            topping.set({ price });
        }
        if (is_active) {
            topping.set({ is_active });
        }
        const toppingUpdate = await topping.save();
        return toppingUpdate;
    }

    async deleteTopping(toppingId) {
        const deleted = await ToppingModel.deleteOne({
            _id: toppingId,
        });
        if (deleted.deletedCount === 1 || deleted.ok === 1) {
            return true;
        }
        return false;
    }

    async getTopping(code) {
        const topping = await ToppingModel.findOne({ code });
        return topping;
    }

    async getToppingById(toppingId) {
        const topping = await ToppingModel.findById(toppingId);
        return topping;
    }

    async getToppings(page, pageSize) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit,
            select: "code image_url name price is_active",
        };
        const toppings = await ToppingModel.paginate({}, options);
        return toppings;
    }

    createNotification(data) {
        return NotificationModel.create(data);
    }

    async updateNotification(notificationId, data) {
        const { title, content, image_url, time_start, time_end } = data;
        const notification = await NotificationModel.findById(notificationId);
        if (!notification) return null;
        if (title) {
            notification.set({ title });
        }
        if (content) {
            notification.set({ content });
        }
        if (image_url) {
            notification.set({ image_url });
        }
        if (time_start) {
            notification.set({ time_start });
        }
        if (time_end) {
            notification.set({ time_end });
        }
        const notificationUpdate = await notification.save();
        return notificationUpdate;
    }

    async deleteNotification(notificationId) {
        const deleted = await NotificationModel.deleteOne({
            _id: notificationId,
        });
        if (deleted.deletedCount === 1 || deleted.ok === 1) {
            return true;
        }
        return false;
    }

    async getNotifications(page, pageSize) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit,
        };
        const notifications = await NotificationModel.paginate({}, options);
        return notifications;
    }

    async getNotificationById(notificationId) {
        const notification = await NotificationModel.findById(notificationId);
        return notification;
    }

    createPromotion(data) {
        return PromotionModel.create(data);
    }

    async updatePromotion(promotionId, data) {
        const {
            code,
            title,
            content,
            image_url,
            value,
            time_start,
            time_end,
        } = data;
        const promotion = await PromotionModel.findById(promotionId);
        if (!promotion) return null;
        if (code) {
            promotion.set({ code });
        }
        if (title) {
            promotion.set({ title });
        }
        if (content) {
            promotion.set({ content });
        }
        if (image_url) {
            promotion.set({ image_url });
        }
        if (time_start) {
            promotion.set({ time_start });
        }
        if (time_end) {
            promotion.set({ time_end });
        }
        if (value) {
            promotion.set({ value });
        }
        const promotionUpdate = await promotion.save();
        return promotionUpdate;
    }

    async deletePromotion(promotionId) {
        const deleted = await PromotionModel.deleteOne({
            _id: promotionId,
        });
        if (deleted.deletedCount === 1 || deleted.ok === 1) {
            return true;
        }
        return false;
    }

    async getPromotions(page, pageSize) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit,
        };
        const promotions = await PromotionModel.paginate({}, options);
        return promotions;
    }

    async getPromotionById(promotionId) {
        const promotion = await PromotionModel.findById(promotionId);
        return promotion;
    }

    async getPromotion(code) {
        const promotion = await PromotionModel.findOne({ code });
        return promotion;
    }

    createConfig(data) {
        return ConfigModel.create(data);
    }

    async updateConfig(key, data) {
        const { value, description } = data;
        const config = await this.getConfig(key);
        if (!config) return null;
        if (value) {
            config.set({ value });
        }
        if (description) {
            config.set({ description });
        }
        const configUpdate = await config.save();
        return configUpdate;
    }

    async deleteConfig(key) {
        const deleted = await ConfigModel.deleteOne({
            key,
        });
        if (deleted.deletedCount === 1 || deleted.ok === 1) {
            return true;
        }
        return false;
    }

    async getConfigs(page, pageSize) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit,
        };
        const configs = await ConfigModel.paginate({}, options);
        return configs;
    }

    async getConfig(key) {
        const config = await ConfigModel.findOne({ key });
        return config;
    }

    async updateOrder(orderId, data) {
        const {
            shipper_id,
            status,
            address,
            lat_addr,
            long_addr,
            promotion_id,
            note,
            drinks,
        } = data;
        const order = await this.getOrder(orderId);
        if (!order) return null;
        if (shipper_id) {
            order.set({ shipper_id });
        }
        if (status) {
            order.set({ status });
            switch (status) {
                case OrderState.Ordered: {
                    order.set({ ordered_time: Date.now() });
                    break;
                }
                case OrderState.Confirmed: {
                    order.set({ confirmed_time: Date.now() });
                    break;
                }
                case OrderState.ToDelivery: {
                    order.set({ to_delivery_time: Date.now() });
                    break;
                }
                case OrderState.Completed: {
                    order.set({ completed_time: Date.now() });
                    break;
                }
                case OrderState.Canceled: {
                    order.set({ canceled_time: Date.now() });
                    break;
                }
            }
        }
        if (address) {
            order.set({ address });
        }
        if (lat_addr) {
            order.set({ lat_addr });
        }
        if (long_addr) {
            order.set({ long_addr });
        }
        if (promotion_id) {
            order.set({ promotion_id });
        }
        if (note) {
            order.set({ note });
        }
        if (drinks) {
            order.set({ drinks });
        }
        const calculatedPrice = await UserHandler.caculatePrice(
            order.drinks,
            order.promotion_id,
            order.lat_addr,
            order.long_addr
        );
        order.set({
            fee_price: {
                fee_ship_per_km: calculatedPrice.feeShipPerKm,
                distance: calculatedPrice.totalDistance,
                total_price: calculatedPrice.totalPrice,
                total_fee_ship: calculatedPrice.totalFeeShip,
                promotion_value: calculatedPrice.promotionValue,
                final_price: calculatedPrice.finalPrice,
            },
        });
        const orderUpdate = await order.save();
        return orderUpdate;
    }

    async deleteOrder(orderId) {
        const deleted = await OrderModel.deleteOne({
            _id: orderId,
        });
        if (deleted.deletedCount === 1 || deleted.ok === 1) {
            return true;
        }
        return false;
    }

    async getOrders(page, pageSize, status, orderId, userId) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit,
        };
        let params = {};
        if (status) {
            params = { ...params, status };
        }
        if (orderId) {
            params = { ...params, _id: orderId };
        }
        if (userId) {
            params = { ...params, user_id: userId };
        }
        const orders = await OrderModel.paginate(
            // status ? { status } : {},
            params,
            options
        );
        return orders;
    }

    async getOrdersOfUser(page, pageSize, userId) {
        const options = {
            page: parseInt(page) || 1,
            limit: parseInt(pageSize) || Pagination.perPageLimit,
        };
        const orders = await OrderModel.paginate({ user_id: userId }, options);
        return orders;
    }

    async getOrder(orderId) {
        const order = await OrderModel.findOne({ _id: orderId });
        return order;
    }

    async getOrderDetail(orderId) {
        const order = await OrderModel.findOne({ _id: orderId })
            .populate("user_id", {}, AccountModel)
            .populate("promotion_id", {}, PromotionModel)
            .populate("drinks.category_id", "", CategoryModel)
            .populate("drinks.toppings", "", ToppingModel);
        return order;
    }
}

export default new AdminHandler();
