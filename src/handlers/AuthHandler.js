import UserModel from '../models/AccountModel';

class AuthHandler {
  async getUserByUsername(username) {
    const user = await UserModel.findOne({ username });
    return user;
  }

  async getUserByPhone(phone) {
    const user = await UserModel.findOne({ phone });
    return user;
  }

  async getUserByEmail(email) {
    const user = await UserModel.findOne({ email });
    return user;
  }

  async getUserByFacebookId(id) {
    const user = await UserModel.findOne({ facebook_id: id });
    return user;
  }

  createNewUser(data) {
    return UserModel.create(data);
  }

  getPayloadJwtSchema(user) {
    return {
      userId: user._id,
      role: user.role,
    };
  }

  async getUserById(userId) {
    // check email lock
    const user = await UserModel.findOne({ _id: userId });
    return user;
  }
}

export default new AuthHandler();
