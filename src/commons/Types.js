export const ExampleType = {
    PROPS1: "PROPS1",
    PROPS2: "PROPS2",
    PROPS3: "PROPS3",
};

// product status
export const ProductStatus = {
    IN_STOCK: "IN_STOCK",
    OUT_STOCK: "OUT_STOCK",
};

export const Pagination = {
    perPageLimit: 20,
};

export const Role = {
    Admin: "admin",
    User: "user",
    Staff: "staff",
};

export const OrderState = {
    Ordered: "ordered",
    Confirmed: "confirmed",
    ToDelivery: "to-delivery",
    Completed: "completed",
    Canceled: "canceled",
};

export const KeyConfig = {
    StoreLocation: "store_location",
    FeeShip: "fee_ship",
};

export const DefaultValue = {
    FeeShip: 5000,
};

export const SocketStatus = {
    Success: "success",
    Error: "error",
    Warning: "warning",
    Info: "info",
};

export const SocketMessage = {
    NewOrder: "You have a new order!",
    UpdateOrder: "Your order was changed status.",
    UpdateProduct: "You have a new product!",
    UpdateTopping: "You have a new topping!",
    UpdateNotification: "You have a new notification!",

    [OrderState.Ordered]: "Bạn đã đặt hàng thành công!",
    [OrderState.Confirmed]: "Đơn hàng của bạn đã được xác nhận!",
    [OrderState.ToDelivery]: "Đơn hàng của bạn đang được giao!",
    [OrderState.Completed]: "Đơn hàng của bạn đã giao thành công!",
    [OrderState.Canceled]: "Đơn hàng của bạn đã bị huỷ.",
};
