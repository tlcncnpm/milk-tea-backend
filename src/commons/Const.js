export const ORDER_STATUS = {
  ORDERED: 'ordered',
  CONFIRMED: 'confirmed',
  TO_DELIVERY: 'to-delivery',
  COMPLETED: 'completed',
  CANCELED: 'canceled',
};
