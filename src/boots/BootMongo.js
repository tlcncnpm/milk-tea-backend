import mongoose from 'mongoose';

import MONGO_URL, { MONGO_USER, MONGO_PASS } from '../config/Database';

export default async () => {
  let link = MONGO_URL;
  if (process.env.NODE_ENV === 'production' && process.env.MONGO_URL) {
    link = process.env.MONGO_URL;
  }
  mongoose.Promise = global.Promise;
  await mongoose.connect(
    link,
    {
      user:
        process.env.NODE_ENV === 'production' && process.env.MONGO_USER
          ? process.env.MONGO_USER
          : MONGO_USER,
      pass:
        process.env.NODE_ENV === 'production' && process.env.MONGO_PASS
          ? process.env.MONGO_PASS
          : MONGO_PASS,
      useNewUrlParser: true,
    },
  );
  console.log('Connect to MongoDB success');
};
